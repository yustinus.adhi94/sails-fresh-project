const { Controller } = require('sails-ember-rest');

TestController = new Controller({
    test: async (req, res) => {
        return res.status(200).send("hello world");
    }
});


module.exports = TestController;