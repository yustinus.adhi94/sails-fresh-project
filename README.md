# sails-fresh-project

a [Sails v1](https://sailsjs.com) application

### Links

+ [Sails framework documentation](https://sailsjs.com/get-started)
+ [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)
+ [Professional / enterprise options](https://sailsjs.com/enterprise)

### How to use

clone this repository

use node 14

install sails 

```
npm i sails -g
```

install dependencies

```
npm i
```

run sails

```
sails lift
```

then go to `localhost:1337` or ``localhost:1337/api/v1/test``
